/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:35:21 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/22 16:36:41 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ftstring.h>
#include <mach-o/nlist.h>

static void	swap(void **a, void **b)
{
	void	*t;

	t = *a;
	*a = *b;
	*b = t;
}

void		sort_64(void *o, struct nlist_64 **arr, uint32_t stroff)
{
	uint32_t			i;
	char				*a;
	char				*b;
	bool				sorted;

	sorted = false;
	while (!sorted)
	{
		i = 0;
		sorted = true;
		while (arr[i + 1])
		{
			a = o + stroff + arr[i]->n_un.n_strx;
			b = o + stroff + arr[i + 1]->n_un.n_strx;
			if (ft_strcmp(a, b) > 0 || (!ft_strcmp(a, b)
						&& arr[i]->n_value < arr[i + 1]->n_value))
			{
				swap((void **)&arr[i], (void **)&arr[i + 1]);
				sorted = false;
			}
			i++;
		}
	}
}

void		sort_32(void *o, struct nlist **arr, uint32_t stroff)
{
	uint32_t			i;
	char				*a;
	char				*b;
	bool				sorted;

	sorted = false;
	while (!sorted)
	{
		i = 0;
		sorted = true;
		while (arr[i + 1])
		{
			a = o + stroff + arr[i]->n_un.n_strx;
			b = o + stroff + arr[i + 1]->n_un.n_strx;
			if (ft_strcmp(a, b) > 0)
			{
				swap((void **)&arr[i], (void **)&arr[i + 1]);
				sorted = false;
			}
			i++;
		}
	}
}
