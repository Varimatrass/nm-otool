/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_command.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 16:32:42 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/21 11:37:26 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "load_command.h"
#include "endianess.h"

#include <ftio.h>
#include <ftstring.h>

#include <mach-o/loader.h>
#include <mach-o/nlist.h>

#define OTOOL g_otool

extern char	*g_name;

void	print(uint32_t i, unsigned int size, char padding)
{
	t_uint	s;

	s = size - count_digit_base(i, 16);
	while (s-- > 0)
		ft_putchar(padding);
	ft_printf("%x", i);
}

void	lc_64_section(void *o, struct section_64 *section,
						struct mach_header_64 *header)
{
	uint32_t	i;
	char		*segname;
	char		*sectname;
	char		*s;

	segname = ft_strndup(section->segname, 16);
	sectname = ft_strndup(section->sectname, 16);
	if (ft_strcmp(sectname, SECT_TEXT) || ft_strcmp(segname, SEG_TEXT))
		return ;
	ft_printf("%s:\n(%s,%s) section", g_name, segname, sectname);
	s = o + E_32(section->offset);
	i = -1;
	while (++i < E_64(section->size))
	{
		if (!(i % 16))
		{
			ft_printf("\n0000000%d", E_32(header->filetype) == MH_EXECUTE);
			print(E_64(section->addr) + i, 8, '0');
			ft_printf(" ");
		}
		print((unsigned char)s[i], 2, '0');
		if (i + 1 < E_64(section->size))
			ft_printf(" ");
	}
	ft_printf("%s\n", E_64(section->size) ? " " : "");
}

void	lc_64_segment(void *o, void *p, struct mach_header_64 *header)
{
	struct segment_command_64	*scmd;
	uint32_t					i;

	i = 0;
	scmd = p;
	p += sizeof(*scmd);
	while (i < E_32(scmd->nsects))
	{
		g_sec_64[g_it++] = p;
		if (OTOOL)
			lc_64_section(o, p, header);
		p += sizeof(struct section_64);
		i++;
	}
}

void	lc_32_section(void *o, struct section *section,
						struct mach_header *header)
{
	uint32_t	i;
	char		*segname;
	char		*sectname;
	char		*s;

	(void)header;
	segname = ft_strndup(section->segname, 16);
	sectname = ft_strndup(section->sectname, 16);
	if (ft_strcmp(sectname, SECT_TEXT) || ft_strcmp(segname, SEG_TEXT))
		return ;
	ft_printf("%s:\n(%s,%s) section", g_name, segname, sectname);
	s = o + E_32(section->offset);
	i = -1;
	while (++i < E_64(section->size))
	{
		if (!(i % 16))
		{
			ft_printf("\n");
			print(E_64(section->addr) + i, 8, '0');
			ft_printf(" ");
		}
		print((unsigned char)s[i], 2, '0');
		ft_printf("%s", i + 1 < E_64(section->size) ? " " : "");
	}
	ft_printf("%s\n", E_64(section->size) ? " " : "");
}

void	lc_32_segment(void *o, void *p, struct mach_header *header)
{
	struct segment_command	*scmd;
	uint32_t				i;

	i = 0;
	scmd = p;
	p += sizeof(*scmd);
	while (i < E_32(scmd->nsects))
	{
		g_sec_32[g_it++] = p;
		if (OTOOL)
			lc_32_section(o, p, header);
		p += sizeof(struct section);
		i++;
	}
}
