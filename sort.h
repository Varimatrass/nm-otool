/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:51:29 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/22 11:54:06 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORT_H
# define SORT_H

struct nlist_64;
struct nlist;

void	sort_64(void *o, struct nlist_64 **arr, uint32_t stroff);
void	sort_32(void *o, struct nlist **arr, uint32_t stroff);

#endif
