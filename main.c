/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 15:16:20 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/22 16:25:26 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "endianess.h"
#include "load_command.h"
#include "main.h"

#include <ftio.h>
#include <ftstring.h>
#include <libft.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <mach-o/loader.h>
#include <mach-o/fat.h>
#include <mach-o/ranlib.h>
#include <mach-o/nlist.h>

#define ARCHIVE_MAGIC "!<arch>"

#define OTOOL g_otool

char				*g_name = NULL;
uint32_t			g_it = 0;
struct section		*g_sec_32[1000];
struct section_64	*g_sec_64[1000];

extern bool	g_otool;

void	dump(void *p)
{
	uint32_t	type;

	type = *(uint32_t *)p;
	if (type == MH_MAGIC)
		o_32(p);
	else if (type == MH_MAGIC_64 || type == MH_CIGAM_64)
	{
		E_SET(type == MH_CIGAM_64);
		o_64(p);
	}
	else if (type == FAT_MAGIC || type == FAT_CIGAM)
	{
		E_SET(true);
		o_fat(p);
	}
	else
	{
		if (OTOOL)
			ft_printf("%s: is not an object file\n", g_name);
	}
}

void	o_32(void *p)
{
	struct mach_header		*header;
	struct load_command		*cmd;
	void					*o;
	uint32_t				i;

	o = p;
	header = p;
	p += sizeof(struct mach_header);
	i = 0;
	while (i < E_32(header->ncmds))
	{
		cmd = p;
		if (E_32(cmd->cmd) == LC_SEGMENT)
			lc_32_segment(o, p, header);
		if (!OTOOL && E_32(cmd->cmd) == LC_SYMTAB)
			lc_32_symtab(o, p, header);
		p += E_32(cmd->cmdsize);
		i++;
	}
}

void	o_64(void *p)
{
	struct mach_header_64	*header;
	struct load_command		*cmd;
	void					*o;
	uint32_t				i;

	o = p;
	header = p;
	p += sizeof(struct mach_header_64);
	i = 0;
	while (i < E_32(header->ncmds))
	{
		cmd = p;
		if (E_32(cmd->cmd) == LC_SEGMENT_64)
			lc_64_segment(o, p, header);
		if (!OTOOL && E_32(cmd->cmd) == LC_SYMTAB)
			lc_64_symtab(o, p, header);
		p += E_32(cmd->cmdsize);
		i++;
	}
}

void	o_fat(void *p)
{
	void				*o;
	struct fat_header	*header;
	struct fat_arch		*arch;
	uint32_t			i;

	o = p;
	header = p;
	p += sizeof(*header);
	i = 0;
	arch = p + sizeof(*arch);
	dump(o + E_32(arch->offset));
}

int		main(int argc, char **argv)
{
	void	*o;
	int		fd;

	g_malloc_use = false;
	if (argc > 1)
	{
		fd = ft_open(argv[1], O_RDONLY);
		g_name = argv[1];
	}
	else
	{
		fd = ft_open("a.out", O_RDONLY);
		g_name = ft_strdup("a.out");
	}
	o = io_map_file(fd, PROT_READ);
	dump(o);
	io_unmap_file(o);
	ft_close(fd);
	return (0);
}
