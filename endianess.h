/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   endianess.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 16:41:11 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/18 16:42:23 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENDIANESS_H
# define ENDIANESS_H

# include <ftendianess.h>

extern bool	g_e;

# define E_16(x) (g_e ? SWAP_16(x) : x)
# define E_32(x) (g_e ? SWAP_32(x) : x)
# define E_64(x) (g_e ? SWAP_64(x) : x)

# define E_SET(b) (g_e = b)

#endif
