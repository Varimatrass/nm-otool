/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symtab_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 15:00:52 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/22 15:29:42 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "load_command.h"

#include <mach-o/loader.h>
#include <mach-o/nlist.h>

#include <ftstring.h>
#include <ftio.h>

#define SPACE_32 "         "

void	print_type_32(uint32_t type, uint32_t sect)
{
	ft_printf("%s", (N_TYPE & type) == N_UNDF ? SPACE_32 : " ");
	if ((N_TYPE & type) == N_UNDF)
		ft_printf("U");
	else if ((N_TYPE & type) == N_SECT)
	{
		if (!ft_strncmp(g_sec_32[sect]->sectname, SECT_TEXT, 16))
			ft_printf("%c", type & N_EXT ? 'T' : 't');
		else if (!ft_strncmp(g_sec_32[sect]->sectname, SECT_DATA, 16))
			ft_printf("%c", type & N_EXT ? 'D' : 'd');
		else if (!ft_strncmp(g_sec_32[sect]->sectname, SECT_BSS, 16))
			ft_printf("%c", type & N_EXT ? 'B' : 'b');
		else if (0 && !ft_strncmp(g_sec_32[sect]->sectname, SECT_COMMON, 16))
			ft_printf("%c", type & N_EXT ? 'C' : 'c');
		else
			ft_printf("%c", type & N_EXT ? 'S' : 's');
	}
}
