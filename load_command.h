/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_command.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 16:34:42 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/22 15:31:57 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOAD_COMMAND_H
# define LOAD_COMMAND_H

# include <stdbool.h>
# include <mach-o/loader.h>

extern uint32_t				g_it;
extern struct section		*g_sec_32[1000];
extern struct section_64	*g_sec_64[1000];
extern bool					g_otool;

void			lc_64_section(void *o, struct section_64 *section,
						struct mach_header_64 *header);

void			lc_64_segment(void *o, void *p, struct mach_header_64 *header);
void			lc_64_symtab(void *o, void *p, struct mach_header_64 *header);

void			lc_32_section(void *o, struct section *section,
						struct mach_header *header);

void			lc_32_segment(void *o, void *p, struct mach_header *header);
void			lc_32_symtab(void *o, void *p, struct mach_header *header);

void			print(uint32_t i, unsigned int size, char padding);

void			print_type_32(uint32_t type, uint32_t sect);

#endif
