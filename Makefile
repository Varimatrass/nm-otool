# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                    +:+ +:+         +:+       #
#    By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+         #
#                                                +#+#+#+#+#+   +#+             #
#    Created: 2013/11/29 09:40:24 by dlancar           #+#    #+#              #
#    Updated: 2016/01/22 15:30:07 by dlancar          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC		= main.c load_command.c symtab.c symtab_2.c endianess.c sort.c
OBJ		= $(SRC:.c=.o)
LIB		= libft
INCL	= $(LIB)
CMP		= cc
LBFLAG	= -lft
FLAGS	= -Wall -Wextra -O0 -g -std=c11

.PHONY: clean fclean re ft_otool ft_nm

all: ft_otool ft_nm

ft_nm: $(OBJ)
	@(cd $(LIB) && $(MAKE))
	@$(CMP) -o ft_nm $(FLAGS) -I./libft -L./libft $(LBFLAG) nm.c $(OBJ)

ft_otool: $(OBJ)
	@(cd $(LIB) && $(MAKE))
	@$(CMP) -o ft_otool $(FLAGS) -I./libft -L./libft $(LBFLAG) otool.c $(OBJ)

%.o: %.c
	@$(CMP) -I $(INCL) -o $@ -c $? $(FLAGS)

clean:
	@(cd $(LIB) && $(MAKE) $@)
	@rm -f $(OBJ)

fclean: clean
	@(cd $(LIB) && $(MAKE) $@)
	@rm -f ft_otool ft_nm

re: fclean all

suicide: fclean
	@rm -f $(SRC)
