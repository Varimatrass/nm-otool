/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symtab.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlancar <dlancar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 15:44:20 by dlancar           #+#    #+#             */
/*   Updated: 2016/01/22 15:35:20 by dlancar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "endianess.h"
#include "load_command.h"
#include "sort.h"

#include <mach-o/nlist.h>

#include <ftstring.h>
#include <ftio.h>

#define SPACE_64 "                 "

struct nlist_64	*g_64_l[10000];
struct nlist	*g_32_l[10000];

static void	print_type_64(uint32_t type, uint32_t sect)
{
	ft_printf("%s", (N_TYPE & type) == N_UNDF ? SPACE_64 : " ");
	if ((N_TYPE & type) == N_UNDF)
		ft_printf("U");
	else if ((N_TYPE & type) == N_SECT)
	{
		if (!ft_strncmp(g_sec_64[sect]->sectname, SECT_TEXT, 16))
			ft_printf("%c", type & N_EXT ? 'T' : 't');
		else if (!ft_strncmp(g_sec_64[sect]->sectname, SECT_DATA, 16))
			ft_printf("%c", type & N_EXT ? 'D' : 'd');
		else if (!ft_strncmp(g_sec_64[sect]->sectname, SECT_BSS, 16))
			ft_printf("%c", type & N_EXT ? 'B' : 'b');
		else if (0 && !ft_strncmp(g_sec_64[sect]->sectname, SECT_COMMON, 16))
			ft_printf("%c", type & N_EXT ? 'C' : 'c');
		else
			ft_printf("S");
	}
}

void		print_64(void *o, struct symtab_command *scmd,
		struct mach_header_64 *header)
{
	struct nlist_64	*nlist;
	uint32_t		i;

	sort_64(o, g_64_l, scmd->stroff);
	i = 0;
	while ((nlist = g_64_l[i]))
	{
		if (nlist->n_type & N_STAB)
		{
			i++;
			continue ;
		}
		if ((N_TYPE & nlist->n_type) != N_UNDF)
		{
			ft_printf("0000000%d", E_32(header->filetype) == MH_EXECUTE);
			print(nlist->n_value, 8, '0');
		}
		print_type_64(nlist->n_type, nlist->n_sect - 1);
		ft_printf(" %s\n", o + scmd->stroff + nlist->n_un.n_strx);
		i++;
	}
}

typedef struct nlist	t_norminetteencarton;

void		print_32(void *o, struct symtab_command *scmd)
{
	t_norminetteencarton	*nlist;
	uint32_t				i;

	sort_32(o, g_32_l, scmd->stroff);
	i = 0;
	while ((nlist = g_32_l[i]))
	{
		if (nlist->n_type & N_STAB)
		{
			i++;
			continue ;
		}
		if ((N_TYPE & nlist->n_type) != N_UNDF)
			print(nlist->n_value, 8, '0');
		print_type_32(nlist->n_type, nlist->n_sect - 1);
		ft_printf(" %s\n", o + scmd->stroff + nlist->n_un.n_strx);
		i++;
	}
}

void		lc_64_symtab(void *o, void *p, struct mach_header_64 *header)
{
	struct symtab_command		*scmd;
	struct nlist_64				*nlist;
	uint32_t					i;

	i = 0;
	scmd = p;
	p = o + scmd->symoff;
	while (i < E_32(scmd->nsyms) && (nlist = p))
	{
		g_64_l[i] = p;
		p += sizeof(struct nlist_64);
		i++;
	}
	print_64(o, scmd, header);
}

void		lc_32_symtab(void *o, void *p, struct mach_header *header)
{
	struct symtab_command		*scmd;
	struct nlist_64				*nlist;
	uint32_t					i;

	(void)header;
	i = 0;
	scmd = p;
	p = o + scmd->symoff;
	while (i < E_32(scmd->nsyms) && (nlist = p))
	{
		g_32_l[i] = p;
		p += sizeof(struct nlist);
		i++;
	}
	print_32(o, scmd);
}
